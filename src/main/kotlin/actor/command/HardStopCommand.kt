package actor.command

import command.ICommand

class HardStopCommand : ICommand {
    override fun execute() = null
}