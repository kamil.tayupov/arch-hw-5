package actor.command

import command.ICommand
import state.IState

class SoftStopCommand(
    private val state: IState
) : ICommand {
    override fun execute() = state
}