package actor.command

import actor.Worker
import command.ICommand
import state.IState
import kotlin.concurrent.thread

class StartWorkerCommand(
    private val worker: Worker,
    private val state: IState
) : ICommand {
    override fun execute(): IState {
        thread {
            worker.start()
        }
        return state
    }
}