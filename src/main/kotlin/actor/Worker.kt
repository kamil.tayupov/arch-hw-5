package actor

import actor.command.HardStopCommand
import actor.command.SoftStopCommand
import command.ICommand
import java.lang.Exception
import java.util.concurrent.BlockingQueue

open class Worker(
    private val tasks: BlockingQueue<ICommand>
) {

    private var stop = false
    private var soft = false

    open fun start() {
        while (!stop) {
            val command = tasks.take()

            if (command is HardStopCommand) {
                stop = true
            }

            if (command is SoftStopCommand) {
                soft = true
            }

            if (soft && tasks.isEmpty()) {
                stop = true
            }

            try {
                command.execute()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}