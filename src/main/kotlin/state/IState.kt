package state

interface IState {
    fun handle(): IState
}