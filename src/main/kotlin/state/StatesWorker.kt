package state

import actor.Worker
import command.ICommand
import java.lang.Exception
import java.util.concurrent.BlockingQueue

class StatesWorker(
    private val tasks: BlockingQueue<ICommand>,
    initialState: IState
) : Worker(tasks) {

    internal var state: IState? = initialState

    override fun start() {
        while (state != null) {
            val command = tasks.take()
            try {
                state = command.execute()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}