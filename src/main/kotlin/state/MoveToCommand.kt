package state

import command.ICommand

class MoveToCommand : ICommand {
    override fun execute(): IState {
        return MoveToState
    }
}