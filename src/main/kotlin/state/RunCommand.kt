package state

import command.ICommand

class RunCommand: ICommand {
    override fun execute(): IState {
        return StraightState
    }
}