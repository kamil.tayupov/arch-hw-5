import data.UObject
import ioc.IoC
import java.lang.reflect.InvocationHandler
import java.lang.reflect.Proxy

object AdapterGenerator {
    @JvmStatic
    @Suppress("UNCHECKED_CAST")
    fun <T> generate(clazz: Class<*>, uObject: UObject): T {
        val invocationHandler = InvocationHandler { _, method, args ->
            return@InvocationHandler if (args == null) {
                IoC.resolve(
                    key = "${clazz.name}.${method.name}",
                    uObject
                )
            } else {
                IoC.resolve(
                    key = "${clazz.name}.${method.name}",
                    uObject,
                    args[0]
                )
            }
        }
        return Proxy.newProxyInstance(ClassLoader.getSystemClassLoader(), arrayOf(clazz), invocationHandler) as T
    }
}