package command

import state.IState

class RotateCommand(
    private val state: IState
) : ICommand {
    override fun execute(): IState = state
}