package command

import data.plus
import operation.Movable
import state.IState

class MoveCommand(
    private val movable: Movable,
    private val state: IState
) : ICommand {
    override fun execute(): IState {
        val position = movable.getPosition()
        val velocity = movable.getVelocity()
        movable.setPosition(position + velocity)
        return state
    }
}