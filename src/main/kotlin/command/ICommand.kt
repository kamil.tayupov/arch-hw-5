package command

import state.IState

interface ICommand {
    fun execute(): IState?
}