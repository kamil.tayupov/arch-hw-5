package command

class MacroCommand(
    private val collection: Collection<ICommand>
): ICommand {
    override fun execute() {
        for (cmd in collection) {
            cmd.execute()
        }
    }
}