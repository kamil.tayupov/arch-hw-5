package command

import operation.Movable
import state.IState

class FinishCommand(
    private val movable: Movable,
    private val state: IState
) : ICommand {
    override fun execute(): IState {
        movable.finish()
        return state
    }
}