package data

interface UObject {
    fun getProperty(name: String): Any
    fun setProperty(name: String, value: Any)
}