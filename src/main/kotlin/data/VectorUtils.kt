package data

operator fun Vector.plus(another: Vector): Vector {
    return Vector(
        this.x + another.x,
        this.y + another.y
    )
}