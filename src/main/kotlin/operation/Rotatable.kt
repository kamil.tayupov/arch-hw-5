package operation

interface Rotatable {
    fun getDirection(): Int
    fun setDirection(value: Int)
    fun getAngularVelocity(): Int
    fun getDirectionsNumber(): Int
}