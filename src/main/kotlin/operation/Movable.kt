package operation

import data.Vector

interface Movable {
    fun getPosition(): Vector
    fun setPosition(value: Vector)
    fun getVelocity(): Vector
    fun finish() = Unit
}