package ioc.command

import command.ICommand
import ioc.data.ScopesRepository
import ioc.data.Strategy
import state.IState

class RegisterDependencyCommand(
    private val key: String,
    private val strategy: Strategy,
    private val state: IState
) : ICommand {
    override fun execute(): IState {
        ScopesRepository.currentScope.get().dependencies[key] = strategy
        return state
    }
}