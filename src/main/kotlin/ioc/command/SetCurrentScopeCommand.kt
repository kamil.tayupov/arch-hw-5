package ioc.command

import command.ICommand
import ioc.data.ScopesRepository
import state.IState

class SetCurrentScopeCommand(
    private val scopeId: String,
    private val state: IState
) : ICommand {
    override fun execute(): IState {
        ScopesRepository.currentScope.set(ScopesRepository.scopes[scopeId])
        return state
    }
}