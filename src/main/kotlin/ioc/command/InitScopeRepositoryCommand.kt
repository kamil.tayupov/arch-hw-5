package ioc.command

import command.ICommand
import data.UObject
import ioc.data.ScopesRepository
import ioc.data.Strategy
import ioc.data.Scope
import operation.Movable
import operation.Rotatable
import state.IState
import state.StraightState
import kotlin.reflect.KCallable
import kotlin.reflect.KClass

class InitScopeRepositoryCommand(
    private val state: IState
) : ICommand {
    override fun execute(): IState {
        if (ScopesRepository.root != null) {
            return state
        }

        val dependencies = mutableMapOf<String, Strategy>()

        val scope = Scope(dependencies)

        dependencies["IoC.Register"] = Strategy { args ->
            RegisterDependencyCommand(
                key = args[0] as String,
                strategy = args[1] as Strategy,
                state
            )
        }

        dependencies["Scopes.New"] = Strategy { args ->
            CreateScopeCommand(
                scopeId = args[0] as String,
                state
            )
        }

        dependencies["Scopes.Current"] = Strategy { args ->
            SetCurrentScopeCommand(
                scopeId = args[0] as String,
                state
            )
        }

        dependencies["Adapter"] = Strategy { args ->
            AdapterGenerator.generate(
                args[0] as Class<*>,
                args[1] as UObject
            )
        }

        registerAdapter(Movable::class, dependencies)
        registerAdapter(Rotatable::class, dependencies)

        ScopesRepository.root = scope

        ScopesRepository.currentScope.set(scope)

        return state
    }

    private fun registerAdapter(clazz: KClass<*>, dependencies: MutableMap<String, Strategy>) {
        for (kCallable in clazz.members) {
            val key = "${clazz.qualifiedName}.${kCallable.name}"

            val strategy = Strategy { args ->
                when (kCallable.type()) {
                    Type.GETTER -> {
                        val obj = (args[0] as UObject)
                        obj.getProperty(kCallable.property())
                    }
                    Type.SETTER -> {
                        val obj = (args[0] as UObject)
                        val value = args[1]
                        obj.setProperty(kCallable.property(), value)
                    }
                    Type.OTHER -> {
                        kCallable.call()!!
                    }
                }
            }
            dependencies[key] = strategy
        }
    }

    private enum class Type {
        GETTER,
        SETTER,
        OTHER;
    }

    private fun KCallable<*>.type(): Type = when {
        name.contains("get") -> Type.GETTER
        name.contains("set") -> Type.SETTER
        else -> Type.OTHER
    }

    private fun KCallable<*>.property(): String {
        return name.substring(3)
    }
}