package ioc.command

import command.ICommand
import ioc.data.ScopesRepository
import ioc.data.Scope
import state.IState

class CreateScopeCommand(
    private val scopeId: String,
    private val state: IState
) : ICommand {
    override fun execute(): IState {
        ScopesRepository.scopes[scopeId] = Scope(mutableMapOf())
        return state
    }
}