package ioc.data

object ScopesRepository {
    internal var root: Scope? = null

    internal var currentScope: ThreadLocal<Scope> = ThreadLocal()
        private set

    internal val scopes = mutableMapOf<String, Scope>()
}