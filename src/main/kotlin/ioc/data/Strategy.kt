package ioc.data

data class Strategy(
    val resolver: (Array<out Any>) -> Any
) {
    fun invoke(params: Array<out Any>) = resolver.invoke(params)
}
