package ioc.data

class Scope(
    internal val dependencies: MutableMap<String, Strategy>
) {
    fun resolve(key: String, args: Array<out Any>): Any? {
        val dependencyResolver = dependencies[key]
        return dependencyResolver?.invoke(args)
    }
}