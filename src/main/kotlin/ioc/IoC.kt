package ioc

import ioc.command.InitScopeRepositoryCommand
import ioc.data.ScopesRepository
import state.StraightState
import java.lang.IllegalArgumentException

@Suppress("UNCHECKED_CAST")
object IoC {

    init {
        InitScopeRepositoryCommand(StraightState).execute()
    }

    fun <T> resolve(key: String, vararg params: Any): T {
        return ScopesRepository.currentScope.get().resolve(key, params) as T
            ?: throw IllegalArgumentException("Dependency \"$key\" was not found")
    }
}