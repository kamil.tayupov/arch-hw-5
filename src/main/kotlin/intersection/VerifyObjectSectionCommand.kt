package intersection

import command.ICommand

class VerifyObjectSectionCommand(
    private val gameObject: GameObject,
    private val sections: Map<Int, Section>
): ICommand {
    override fun execute() {
        val currentSectionId = gameObject.sectionId
        if (!gameObject.insideSpecifiedSection(sections, currentSectionId)) {
            SpecifyObjectSectionCommand(gameObject, sections).execute()
            val verifiedSectionId = gameObject.sectionId
            sections[currentSectionId]?.remove(gameObject)
            sections[verifiedSectionId]?.add(gameObject)
        }
    }
}

