package intersection

interface Intersectable {
    fun getSectionId(): Int
    fun setSectionId(value: Int)
}