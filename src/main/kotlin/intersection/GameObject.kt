package intersection

import data.Vector
import kotlin.random.Random

data class GameObject(
    private val id: Int = Random.nextInt(),
    var position: Vector
) {
    val x get() = position.x
    val y get() = position.y

    var sectionId: Int = -1
}