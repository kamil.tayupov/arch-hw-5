package intersection

import command.ICommand
import command.MacroCommand

class CheckMultipleSectionsSetIntersectionCommand(
    private val mapSet: Set<Map<Int, Section>>,
    private val checker: (GameObject, GameObject) -> Boolean,
    private val consumer: () -> Unit
) : ICommand {
    override fun execute() {
        val commands = mapSet.map { it.values }.flatten().map {
            CheckSectionObjectsIntersectionCommand(it, checker, consumer)
        }
        MacroCommand(commands).execute()
    }
}