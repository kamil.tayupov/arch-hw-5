package intersection

import command.ICommand

class SpecifyObjectSectionCommand(
    private val gameObject: GameObject,
    private val sections: Map<Int, Section>
) : ICommand {
    override fun execute() {
        val sectionId = sections.keys.find {
            gameObject.insideSpecifiedSection(sections, it)
        } ?: throw IndexOutOfBoundsException("Object $gameObject is out of sections $sections")
        gameObject.sectionId = sectionId
    }
}