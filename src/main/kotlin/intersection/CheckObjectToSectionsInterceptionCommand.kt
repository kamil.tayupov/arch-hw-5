package intersection

import command.ICommand
import command.MacroCommand

class CheckObjectToSectionsInterceptionCommand(
    private val gameObject: GameObject,
    private val sections: Map<Int, Section>,
    private val checker: (GameObject, GameObject) -> Boolean,
    private val consumer: () -> Unit
) : ICommand {
    override fun execute() {
        VerifyObjectSectionCommand(gameObject, sections).execute()
        val section = sections[gameObject.sectionId] ?: throw IndexOutOfBoundsException("Section with id ${gameObject.sectionId} was not found in sections $sections")
        val commands = section.list.filter { it != gameObject }.map {
            CheckObjectToObjectIntersectionCommand(it, gameObject, checker, consumer)
        }
        MacroCommand(commands).execute()
    }
}