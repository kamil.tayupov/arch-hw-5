package intersection

fun GameObject.insideSpecifiedSection(
    sections: Map<Int, Section>,
    sectionId: Int
): Boolean {
    val section = sections[sectionId] ?: throw IndexOutOfBoundsException("Section with id $sectionId was not found in sections $sections")
    val topLeft = section.topLeft
    val bottomRight = section.bottomRight

    return x >= topLeft.x && y >= topLeft.y
            && x < bottomRight.x && y < bottomRight.y
}