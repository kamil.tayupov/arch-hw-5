package intersection

import command.ICommand

class CheckObjectToObjectIntersectionCommand(
    private val obj1: GameObject,
    private val obj2: GameObject,
    private val checker: (GameObject, GameObject) -> Boolean,
    private val consumer: () -> Unit
): ICommand {
    override fun execute() {
        if (checker.invoke(obj1, obj2)) {
            consumer.invoke()
        }
    }
}