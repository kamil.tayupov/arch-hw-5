package intersection

import command.ICommand
import command.MacroCommand

class CheckSectionObjectsIntersectionCommand(
    private val section: Section,
    private val checker: (GameObject, GameObject) -> Boolean,
    private val consumer: () -> Unit
) : ICommand {
    override fun execute() {
        val objects = section.list
        if (objects.size < 2) return

        val commands = mutableListOf<ICommand>()
        for (i in 0..<objects.size - 1) {
            for (j in i+1..<objects.size) {
                commands.add(CheckObjectToObjectIntersectionCommand(objects[i], objects[j], checker, consumer))
            }
        }
        MacroCommand(commands).execute()
    }
}