package intersection

import data.Vector

data class Section(
    val topLeft: Vector,
    val bottomRight: Vector
) {

    internal val list = mutableListOf<GameObject>()

    fun add(gameObject: GameObject) {
        list.add(gameObject)
    }

    fun remove(gameObject: GameObject) {
        list.remove(gameObject)
    }
}