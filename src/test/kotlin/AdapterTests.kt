import command.FinishCommand
import command.MoveCommand
import mock.MovableObject
import data.UObject
import data.Vector
import ioc.IoC
import operation.Movable
import operation.Rotatable
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class AdapterTests {

    @Test
    fun `should return movable adapter` () {
        val adapter = IoC.resolve<Any>("Adapter", Movable::class.java, mock<UObject>())
        assertTrue { adapter is Movable }
    }

    @Test
    fun `should return rotatable adapter` () {
        val adapter = IoC.resolve<Any>("Adapter", Rotatable::class.java, mock<UObject>())
        assertTrue { adapter is Rotatable }
    }

    @Test
    fun `should return current position` () {
        val obj = MovableObject(
            position = Vector(12, 5),
            velocity = Vector(-7, 3)
        )
        val adapter = IoC.resolve<Movable>("Adapter", Movable::class.java, obj)
        assertEquals(Vector(12, 5), adapter.getPosition())
    }

    @Test
    fun `should return current velocity` () {
        val obj = MovableObject(
            position = Vector(12, 5),
            velocity = Vector(-7, 3)
        )
        val adapter = IoC.resolve<Movable>("Adapter", Movable::class.java, obj)
        assertEquals(Vector(-7, 3), adapter.getVelocity())
    }

    @Test
    fun `should move object`() {
        val obj = MovableObject(
            position = Vector(12, 5),
            velocity = Vector(-7, 3)
        )
        val adapter = IoC.resolve<Movable>("Adapter", Movable::class.java, obj)

        val command = MoveCommand(adapter, mock())
        command.execute()

        assertEquals(Vector(5, 8), adapter.getPosition())
    }

    // пытался сделать сделать 3*, но не смог (((
    @Test
    fun `should call finish`() {
        val adapter = IoC.resolve<Movable>("Adapter", Movable::class.java, mock<UObject>())

        val command = FinishCommand(adapter, mock())
        command.execute()

        verify(adapter).finish()
    }
}