import command.ICommand
import command.MoveCommand
import ioc.*
import ioc.command.CreateScopeCommand
import ioc.command.RegisterDependencyCommand
import ioc.command.SetCurrentScopeCommand
import ioc.data.ScopesRepository
import ioc.data.Strategy
import org.junit.jupiter.api.assertThrows
import org.mockito.Mockito.mock
import java.lang.IllegalArgumentException
import kotlin.test.*

class IoCTests {

    @Test
    fun `should return register command`() {
        val resolve = IoC.resolve<Any>(
            key = "IoC.Register",
            "",
            Strategy { _ -> Any() }
        )

        assertTrue { resolve is RegisterDependencyCommand }
    }

    @Test
    fun `should throw exception for unregistered command`() {
        assertThrows<IllegalArgumentException> {
            IoC.resolve<Any>(
                key = "UnregisteredCommand"
            )
        }
    }

    @Test
    fun `should return concrete command after registration`() {
        IoC.resolve<ICommand>(
            key = "IoC.Register",
            "MoveCommand",
            Strategy { _ -> MoveCommand(mock(), mock()) }
        ).execute()

        val resolve = IoC.resolve<Any>(
            key = "MoveCommand"
        )

        assertTrue { resolve is MoveCommand }
    }

    @Test
    fun `should return new scope`() {
        val resolve = IoC.resolve<Any>(
            key = "Scopes.New",
            "ScopeId"
        )

        assertTrue { resolve is CreateScopeCommand }

        assertNull(ScopesRepository.scopes["ScopeId"])

        (resolve as CreateScopeCommand).execute()

        assertNotNull(ScopesRepository.scopes["ScopeId"])
    }

    @Test
    fun `should return current scope`() {
        IoC.resolve<ICommand>(
            key = "Scopes.New",
            "ScopeId"
        ).execute()

        val resolve = IoC.resolve<Any>(
            key = "Scopes.Current",
            "ScopeId"
        )

        assertTrue { resolve is SetCurrentScopeCommand }

        assertNotEquals(ScopesRepository.scopes["ScopeId"], ScopesRepository.currentScope.get())

        (resolve as SetCurrentScopeCommand).execute()

        assertEquals(ScopesRepository.scopes["ScopeId"], ScopesRepository.currentScope.get())
    }
}