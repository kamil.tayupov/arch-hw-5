import actor.Worker
import actor.command.HardStopCommand
import actor.command.SoftStopCommand
import actor.command.StartWorkerCommand
import command.*
import mock.CountDownLatchCommand
import mock.ThrowExceptionCommand
import org.mockito.Mockito.mock
import java.util.concurrent.CountDownLatch
import java.util.concurrent.LinkedBlockingQueue
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class ActorModelTests {

    @Test
    fun `should start worker`() {
        val queue = LinkedBlockingQueue<ICommand>()
        val worker = Worker(queue)
        val latch = CountDownLatch(1)

        queue.add(MoveCommand(mock(), mock()))
        queue.add(CountDownLatchCommand(latch))

        StartWorkerCommand(worker, mock()).execute()

        latch.await()
        assertTrue { queue.isEmpty() }
    }

    @Test
    fun `should continue working after exception`() {
        val queue = LinkedBlockingQueue<ICommand>()
        val worker = Worker(queue)
        val latch = CountDownLatch(1)

        queue.add(MoveCommand(mock(), mock()))
        queue.add(ThrowExceptionCommand())
        queue.add(RotateCommand(mock()))
        queue.add(CountDownLatchCommand(latch))

        StartWorkerCommand(worker, mock()).execute()

        latch.await()
        assertTrue { queue.isEmpty() }
    }

    @Test
    fun `should hard stop workers`() {
        val queue = LinkedBlockingQueue<ICommand>()
        val worker = Worker(queue)

        queue.add(MoveCommand(mock(), mock()))
        queue.add(RotateCommand(mock()))
        queue.add(HardStopCommand())
        queue.add(MoveCommand(mock(), mock()))

        StartWorkerCommand(worker, mock()).execute()

        Thread.sleep(1000)
        assertEquals(1, queue.size)
    }

    @Test
    fun `should soft stop workers`() {
        val queue = LinkedBlockingQueue<ICommand>()
        val worker = Worker(queue)
        val latch = CountDownLatch(1)

        queue.add(MoveCommand(mock(), mock()))
        queue.add(RotateCommand(mock()))
        queue.add(SoftStopCommand(mock()))
        queue.add(RotateCommand(mock()))
        queue.add(CountDownLatchCommand(latch))

        StartWorkerCommand(worker, mock()).execute()

        latch.await()
        assertTrue { queue.isEmpty() }

        queue.add(RotateCommand(mock()))
        Thread.sleep(1000)
        assertEquals(1, queue.size)
    }
}