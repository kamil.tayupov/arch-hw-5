package mock

import data.UObject
import data.Vector

class MovableObject(
    position: Vector? = null,
    velocity: Vector? = null
) : UObject {

    private val properties: MutableMap<String, Any?> = mutableMapOf(
        "Position" to position,
        "Velocity" to velocity,
    )

    override fun getProperty(name: String): Any {
        return properties[name] ?: throw IllegalArgumentException()
    }

    override fun setProperty(name: String, value: Any) {
        properties[name] = value
    }
}