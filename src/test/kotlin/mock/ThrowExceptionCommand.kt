package mock

import command.ICommand
import state.IState

class ThrowExceptionCommand : ICommand {
    override fun execute(): IState {
        throw Exception()
    }
}