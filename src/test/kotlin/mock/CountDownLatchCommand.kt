package mock

import command.ICommand
import state.IState
import state.StraightState
import java.util.concurrent.CountDownLatch

class CountDownLatchCommand(
    private val latch: CountDownLatch
) : ICommand {
    override fun execute(): IState {
        latch.countDown()
        return StraightState
    }
}