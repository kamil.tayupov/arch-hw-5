package mock

import data.UObject

class IntersectableObject(
    sectionId: Int? = null
) : UObject {

    private val properties: MutableMap<String, Any?> = mutableMapOf(
        "SectionId" to sectionId
    )

    override fun getProperty(name: String): Any {
        return properties[name] ?: throw IllegalArgumentException()
    }

    override fun setProperty(name: String, value: Any) {
        properties[name] = value
    }
}