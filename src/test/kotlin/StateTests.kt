import state.StatesWorker
import actor.command.HardStopCommand
import actor.command.StartWorkerCommand
import command.ICommand
import mock.CountDownLatchCommand
import mock.MockState
import org.mockito.Mockito.mock
import state.MoveToCommand
import state.MoveToState
import state.RunCommand
import state.StraightState
import java.util.concurrent.CountDownLatch
import java.util.concurrent.LinkedBlockingQueue
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class StateTests {

    @Test
    fun `should invoke command form queue`() {
        val queue = LinkedBlockingQueue<ICommand>()
        val worker = StatesWorker(queue, mock())
        val latch = CountDownLatch(1)

        queue.add(RunCommand())
        queue.add(CountDownLatchCommand(latch))

        StartWorkerCommand(worker, mock()).execute()

        latch.await()
        assertTrue { queue.isEmpty() }
    }

    @Test
    fun `should hard stop worker`() {
        val queue = LinkedBlockingQueue<ICommand>()
        val worker = StatesWorker(queue, mock())

        queue.add(RunCommand())
        queue.add(RunCommand())
        queue.add(HardStopCommand())
        queue.add(RunCommand())

        StartWorkerCommand(worker, mock()).execute()

        Thread.sleep(1000)
        assertEquals(1, queue.size)
    }

    @Test
    fun `should return move to state`() {
        val queue = LinkedBlockingQueue<ICommand>()
        val initialState = MockState
        val worker = StatesWorker(queue, initialState)

        assertEquals(initialState, worker.state)

        queue.add(MoveToCommand())

        StartWorkerCommand(worker, mock()).execute()

        Thread.sleep(1000)
        assertTrue(worker.state is MoveToState)
    }


    @Test
    fun `should return straight state after run command`() {
        val queue = LinkedBlockingQueue<ICommand>()
        val initialState = MockState
        val worker = StatesWorker(queue, initialState)

        assertEquals(initialState, worker.state)

        queue.add(RunCommand())

        StartWorkerCommand(worker, mock()).execute()

        Thread.sleep(1000)
        assertTrue(worker.state is StraightState)
    }
}