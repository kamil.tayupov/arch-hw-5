import data.Vector
import intersection.*
import org.junit.jupiter.api.assertThrows
import org.mockito.Mockito.*
import kotlin.math.abs
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class IntersectionTests {

    private val consumer: () -> Unit = mock()

    // Mock for proper intersection check
    private object Checker : (GameObject, GameObject) -> Boolean {
        override fun invoke(aObject: GameObject, bObject: GameObject): Boolean {
            return abs(aObject.x - bObject.x) <= 2 || abs(aObject.y - bObject.y) <= 2
        }
    }

    @Test
    fun `should catch intersection when objects have same position`() {
        val someObject = GameObject(position = Vector(0, 0))
        val anotherObject = GameObject(position = Vector(0, 0))

        CheckObjectToObjectIntersectionCommand(someObject, anotherObject, Checker, consumer).execute()

        verify(consumer).invoke()
    }

    @Test
    fun `should catch intersection when objects are close to each other`() {
        val someObject = GameObject(position = Vector(0, 0))
        val anotherObject = GameObject(position = Vector(2, 2))

        CheckObjectToObjectIntersectionCommand(someObject, anotherObject, Checker, consumer).execute()

        verify(consumer).invoke()
    }

    @Test
    fun `should not catch intersection when objects are far from each other`() {
        val someObject = GameObject(position = Vector(0, 0))
        val anotherObject = GameObject(position = Vector(3, 3))

        CheckObjectToObjectIntersectionCommand(someObject, anotherObject, Checker, consumer).execute()

        verifyNoInteractions(consumer)
    }

    @Test
    fun `should specify object's section`() {
        val section = Section(topLeft = Vector(0, 0), bottomRight = Vector(10, 10))
        val gameObject = GameObject(position = Vector(8, 8))

        val sections = mapOf(42 to section)
        SpecifyObjectSectionCommand(gameObject, sections).execute()

        assertEquals(42, gameObject.sectionId)
    }

    @Test
    fun `should not refer border object to left section`() {
        val section = Section(topLeft = Vector(0, 0), bottomRight = Vector(10, 10))
        val gameObject = GameObject(position = Vector(10, 8))

        val sections = mapOf(42 to section)

        assertThrows<IndexOutOfBoundsException> {
            SpecifyObjectSectionCommand(gameObject, sections).execute()
        }
    }

    @Test
    fun `should not refer border object to top section`() {
        val section = Section(topLeft = Vector(0, 0), bottomRight = Vector(10, 10))
        val gameObject = GameObject(position = Vector(8, 10))

        val sections = mapOf(42 to section)

        assertThrows<IndexOutOfBoundsException> {
            SpecifyObjectSectionCommand(gameObject, sections).execute()
        }
    }

    @Test
    fun `should refer border object to bottom right section`() {
        val section = Section(topLeft = Vector(10, 10), bottomRight = Vector(20, 20))
        val gameObject = GameObject(position = Vector(10, 10))

        val sections = mapOf(42 to section)
        SpecifyObjectSectionCommand(gameObject, sections).execute()

        assertEquals(42, gameObject.sectionId)
    }

    @Test
    fun `should change object's section`() {
        val currentSection = Section(topLeft = Vector(0, 0), bottomRight = Vector(10, 10))
        val anotherSection = Section(topLeft = Vector(10, 10), bottomRight = Vector(20, 20))

        val gameObject = GameObject(position = Vector(13, 13)).apply { sectionId = 42 }
        currentSection.add(gameObject)

        val sections = mapOf(42 to currentSection, 57 to anotherSection)
        VerifyObjectSectionCommand(gameObject, sections).execute()

        assertEquals(57, gameObject.sectionId)
        assertTrue(currentSection.list.isEmpty())
        assertFalse(anotherSection.list.isEmpty())
    }

    @Test
    fun `should catch object to section interception`() {
        val someSection = Section(topLeft = Vector(0, 0), bottomRight = Vector(10, 10))
        val someObject = GameObject(position = Vector(8, 8)).apply { sectionId = 42 }
        someSection.add(someObject)

        val anotherSection = Section(topLeft = Vector(10, 10), bottomRight = Vector(20, 20))
        val anotherObject = GameObject(position = Vector(9, 9)).apply { sectionId = 57 }
        anotherSection.add(anotherObject)

        val sections = mapOf(42 to someSection, 57 to anotherSection)
        CheckObjectToSectionsInterceptionCommand(anotherObject, sections, Checker, consumer).execute()

        verify(consumer).invoke()
    }

    @Test
    fun `should not catch object to section interception`() {
        val someSection = Section(topLeft = Vector(0, 0), bottomRight = Vector(10, 10))
        val someObject = GameObject(position = Vector(8, 8)).apply { sectionId = 42 }
        someSection.add(someObject)

        val anotherSection = Section(topLeft = Vector(10, 10), bottomRight = Vector(20, 20))
        val anotherObject = GameObject(position = Vector(13, 13)).apply { sectionId = 57 }
        anotherSection.add(anotherObject)

        val sections = mapOf(42 to someSection, 57 to anotherSection)
        CheckObjectToSectionsInterceptionCommand(anotherObject, sections, Checker, consumer).execute()

        verifyNoInteractions(consumer)
    }

    @Test
    fun `should catch set of map sections interception`() {
        val sections = createSections(
            fromPoint = Vector(0, 0),
            step = 10,
            dimension = 2,
            initialId = 42
        )

        sections.locate(GameObject(position = Vector(9, 9)))
        sections.locate(GameObject(position = Vector(9, 11)))
        sections.locate(GameObject(position = Vector(11, 9)))
        sections.locate(GameObject(position = Vector(11, 11)))

        CheckMultipleSectionsSetIntersectionCommand(setOf(sections), Checker, consumer).execute()

        verifyNoInteractions(consumer)

        val anotherSections = createSections(
            fromPoint = Vector(5, 5),
            step = 10,
            dimension = 2,
            initialId = 57
        )

        anotherSections.locate(GameObject(position = Vector(9, 9)))
        anotherSections.locate(GameObject(position = Vector(11, 11)))

        CheckMultipleSectionsSetIntersectionCommand(setOf(anotherSections), Checker, consumer).execute()

        verify(consumer, times(1)).invoke()
    }

    private fun createSections(
        fromPoint: Vector,
        step: Int,
        dimension: Int,
        initialId: Int
    ): Map<Int, Section> {
        val sections = mutableMapOf<Int, Section>()

        var id = initialId
        for (i in 0..<dimension) {
            var topLeft = fromPoint.copy(x = fromPoint.x + i * step)
            for (j in 0..<dimension) {
                topLeft = topLeft.copy(y = fromPoint.y + j * step)
                val bottomRight = topLeft.copy(x = topLeft.x + step, y = topLeft.y + step)
                sections[id++] = Section(topLeft, bottomRight)
            }
        }

        return sections
    }

    private fun Map<Int, Section>.locate(gameObject: GameObject) {
        SpecifyObjectSectionCommand(gameObject, this).execute()
        this[gameObject.sectionId]?.add(gameObject)
    }
}
